#!/bin/sh
#
#  install.sh
sudo apt install -y python3-pip
pip3 install numpy
sudo add-apt-repository ppa:alex-p/tesseract-ocr
sudo apt-get update
sudo apt install -y tesseract-ocr
export PYSPARK_PYTHON=python3
export PYSPARK_DRIVER_PYTHON=python3
sudo rm requirements.txt
hadoop fs -copyToLocal requirements.txt requirements.txt
pip3 install -r requirements.txt