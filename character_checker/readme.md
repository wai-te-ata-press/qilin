# Character Checker

This interface is designed for use in checking the accuracy of character identification.

## Getting Started
### Installing
Download character_checker.zip and unzip into desired location. Check that interface.exe, record.csv, and UTF2.zip are present. Unzip UTF2.zip into a folder in the same place named UTF2. Make sure the image folder is named UTF2 and the other file is named record.csv.

## Usage
Run interface.exe by double clicking on the icon. This should bring up a window with an image, a unicode character, and two buttons. Note: the program may take about 10 seconds to open, do not be alarmed if it does not run immediately. 

From there, you can mark the images as correct or not. If at any point you wish to take a break, you can close the program and when re-opened it will pick up where you left off. 

When all images are identified, a pop-up will inform you. To return the data, simply send record.csv to the project team.