from PIL import Image
import time
import numpy as np
from qilin.characteranalysis.basic_classifier import load, predict,save
from qilin.characteranalysis.basic_classifier import  train_classifiers as train
clusters=4

kmeans, classifiers=train(sub_clusters=clusters,svcs=30)
save(str(clusters)+"version.pkl",kmeans,classifiers)
exit()
kmeans, classifiers = load(str(clusters)+"version.pkl")
print("loaded")
# kmeans and classifiers can now be passed to basic_classifier.predict to get a prediction
for i in range(10,20):
    tmp = Image.open("resources/examples/"+str(i+1)+".png").convert('L')
    pred = predict(kmeans, classifiers, tmp)
    preds=[Image.open("resources/catalog_characters/"+str(p)+".png") for p in pred]
    tmp.show()
    time.sleep(10)
    for p in preds:
        p.show()
        time.sleep(5)
    inpt=input()
    if(inpt=='y'):
        tmp.save(str(pred)+".png")




# These lines will retrain the classifiers and save it again
# kmeans, classifiers = train_classifiers()
# save("basic_classifiers.pkl", kmeans, classifiers)
