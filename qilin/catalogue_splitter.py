from typing import List

import math
import numpy as np
from PIL import Image, ImageDraw


def find_gradient(points):
    gradient = 0
    for j in range(len(points) - 1):
        x0, y0 = points[j]
        x1, y1 = points[j + 1]
        gradient += (x1 - x0) / (y1 - y0)
    gradient /= len(points)
    return gradient


def __calculate_angle(top_point):
    gradient = find_gradient(top_point)
    theta = math.atan(gradient)
    return np.rad2deg(-theta)


def __trim_outliers(points):
    xs = []
    for x, _ in points:
        xs.append(x)
    points_copy = points.copy()

    for x, y in points_copy:
        median = np.median(xs)
        if np.abs(x - median) > 10:
            points.remove((x, y))
            xs.remove(x)

    return points


def realign_image(img: Image.Image, im_index, sam, save_loc, save=True) -> List[Image.Image]:
    """
    calculate gradient of side of box
    set rotation angle
    split the catalog page into rows
    :param img: the image
    :param im_index: index of the page of the catalog
    :param sam: The type of catalog
    :param save_loc: location to save rows to
    :param: save: Whether to save the output
    :return: list of images for each row
    """

    width, height = img.size
    img.getpixel((0, 0))

    gray = img.convert('L')
    bw = gray.point(lambda z: 0 if z < 128 else 255, '1')
    img = bw

    offset = 210 if not sam else 180
    end = int(width/2) if not sam else int(width/10)
    steps = 50

    if img.getpixel((50, 50)) == 255 and not sam:
        offset = 500
        steps = 1000

    colour = img.convert("RGB")
    draw = ImageDraw.Draw(colour)

    line_width = 3 if not sam else 0

    left_point = []
    for y in range(int(height / 3), int(2 * height / 3), int(height / steps)):
        for x in range(offset, end):
            g = img.getpixel((x, y))
            g1 = img.getpixel((x + line_width, y))
            if g == 0 and g1 == 0:
                left_point.append((x, y))
                draw.ellipse((x - 5, y - 5, x + 5, y + 5), fill=(255, 0, 0))
                break

    # colour.show()
    left_point = __trim_outliers(left_point)

    for x, y in left_point:
        draw.ellipse((x - 5, y - 5, x + 5, y + 5), fill=(0, 0, 255))

    theta = __calculate_angle(left_point)

    x = 500
    grad = find_gradient(left_point)
    for y in range(0, height):
        draw.ellipse((x - 5, y - 5, x + 5, y + 5), fill=(0, 255, 255))
        x += grad

    # colour.show()

    img = img.rotate(theta)
    colour = img.convert("RGB")
    draw = ImageDraw.Draw(colour)
    width, height = img.size

    left_point = []
    for y in range(int(height / 4), int(4 * height / 5), 1):  # int(height / steps)):
        for x in range(offset, end, 1):
            g = img.getpixel((x, y))
            g1 = img.getpixel((x + 3, y))
            if g == 0 and g1 == 0:
                left_point.append(x)
                draw.ellipse((x - 5, y - 5, x + 5, y + 5), fill=(255, 0, 255))
                break

    left = np.median(left_point)

    # colour.show()

    img = img.crop((left, 0, width, height))

    width, height = img.size

    right_point = []

    for y in range(int(height / 2), height, int(height / 10)):
        white_found = False
        for x in range(width - 50, int(width / 2), -1):
            g = img.getpixel((x, y))
            g1 = img.getpixel((x - 1, y))
            if not white_found:
                white_found = g == 255 and g1 == 255
            elif g == 0 and g1 == 0:
                right_point.append(x)
                break

    right = int(np.median(right_point))

    top_points = []

    for x in range(20, width, int(width / 10)):
        white_found = False
        for y in range(100, int(height / 2)):
            g = img.getpixel((x, y))
            g1 = img.getpixel((x - 20, y))
            if not white_found:
                white_found = g == 255 and g1 == 255
            elif g == 0 and g1 == 0:
                top_points.append(y)
                break

    top = int(np.median(top_points))

    white_found = False
    for y in range(100, int(height / 2)):
        x = int(right / 2)
        g = img.getpixel((x, y))
        g1 = img.getpixel((x - 100, y))
        if not white_found:
            white_found = g == 255 and g1 == 255
        elif g == 0 and g1 == 0:
            top = y
            break

    bot_points = []

    for j in range(0, 4, 3):
        quarter = int(right / 4)
        for x in range(j * quarter, (j + 1) * quarter, int(right / 40)):
            white_found = False
            for y in range(height - 100, int(height / 2), -1):
                g = img.getpixel((x, y))
                g1 = img.getpixel((x, y))
                if not white_found:
                    white_found = g == 255 and g1 == 255
                elif g == 0 and g1 == 0:
                    bot_points.append(y)
                    break

    bot = int(np.median(bot_points))

    img = img.crop((0, top, right, bot))
    # img.show()

    width, height = img.size

    rows = []

    trim_sides = int(height / 21 * 0.125)

    for j in range(0, 7):
        start = int(j * (height / 7))
        end = start + int(2 * height / 21)

        row = img.crop((trim_sides, start, width - trim_sides, end))
        index = (("0" + repr(im_index)) if im_index < 10 else repr(im_index))
        if sam:
            row_name = "Sam_" + index + "-row" + repr(j) + ".png"
            if save:
                row.save(save_loc + "new_catalogue/" + row_name)
        else:
            row_name = "UTF" + index + "-row" + repr(j) + ".png"
            if save:
                row.save(save_loc + "old_catalogue/" + row_name)
        rows.append(row)

    # img.show()
    return rows


def split_rows(images, sam, page, save_loc, save=True):
    num_chars = 17 if sam else 18
    im_index = 0
    chars = []
    for image in images:
        width, height = image.size
        for j in range(0, num_chars):
            start = int(j * (width / num_chars))
            char = image.crop((start, 0, start + int(width / num_chars), height))

            c_width, c_height = char.size
            top_points = []
            top = 0

            for x in range(10, c_width):
                black_found = False
                for y in range(0, int(c_height / 5)):
                    g = char.getpixel((x, y))
                    g1 = char.getpixel((x-10, y))
                    if (not black_found) and g == 0 and g1 == 0:
                        black_found = True
                    elif black_found and g == 255 and g1 == 255:
                        top_points.append(y)
                        break

            while top_points and max(top_points) - np.median(top_points) > 15:
                top_points.remove(max(top_points))

            threshold = c_width/4 if not sam else c_width/2

            if len(top_points) > threshold:
                top = max(top_points)

            bot_points = []

            for x in range(10, c_width):
                black_found = False
                for y in range(c_height - 1, int(c_height * 3/4), -1):
                    g = char.getpixel((x, y))
                    g1 = char.getpixel((x-10, y))
                    if (not black_found) and g == 0 and g1 == 0:
                        black_found = True
                    elif black_found and g == 255 and g1 == 255:
                        bot_points.append(y)
                        break

            bot = c_height

            while bot_points and np.median(bot_points) - min(bot_points) > 15:
                bot_points.remove(min(bot_points))

            if bot_points:  # c_width/2:
                bot = min(bot_points)

            char = char.crop((0, top, c_width, bot))

            page_index = (("0" + repr(page)) if page < 10 else repr(page))
            index = (("00" + repr(im_index)) if im_index < 10
                     else "0" + repr(im_index) if im_index < 100 else repr(im_index))
            if sam:
                char_name = "Sam_" + page_index + "character" + index + ".png"
                if save:
                    char.save(repr(save_loc) + "new_catalogue/" + repr(char_name), format='PNG')
            else:
                char_name = "UTF" + page_index + "-character" + index + ".png"
                if save:
                    char.save(repr(save_loc) + "old_catalogue/" + repr(char_name), format='PNG')
            chars.append(char)
            im_index += 1
    return chars


if __name__ == '__main__':
    new_catalogue_path = "../resources/tifs/new_catalogue/"
    old_catalogue_path = "../resources/tifs/"
    save_path = "../resources/tifs/characters/"
    new_catalogue = False
    num_pages = 67 if new_catalogue else 60
    for i in range(1, num_pages + 1):
        if new_catalogue:
            name = "Sam_ (" + repr(i) + ").png"
            name = new_catalogue_path + name
        else:
            name = "UTF" + (("0" + repr(i)) if i < 10 else repr(i)) + ".tiff"
            name = old_catalogue_path + name
        im_arr = Image.open(name)
        image_data = realign_image(im_arr, i, new_catalogue, save_loc=save_path)
        split_rows(image_data, new_catalogue, i, save_loc=save_path)
