import unittest
from qilin.splitter.generate_unicode import get_training_set, ch_common


class TestUnicodeGenerator(unittest.TestCase):

    def test_dimensions(self):
        data = get_training_set((5, 5), ch_common, dataset_size=1)[0]
        self.assertEqual((1, 5, 5), data.shape)

    def test_dimensions_full_set(self):
        data = get_training_set((32, 32), ch_common, dataset_size=1)[0]
        self.assertEqual((data.shape[0], 32, 32), data.shape)

if __name__ == '__main__':
    unittest.main()
