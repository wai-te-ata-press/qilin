""" Sequential Main
This is a script mainly for testing purposes. It runs the splitter then
Tesseract on only one image to get an idea of the accuracy that
the system has.

There is various debugging output to different files at diffrent stages
of the process eg.

cam_sections.png: Image with the different sections that the splitter
found highlighted

predicted_regions: Image showing red boxes for the sections that were
cut out of the newspaper scan

out_{#}_{Body|Subheading|Headline}.png: The regions that were cropped
out and passed to Tesseract

"""
# pylint: disable=wrong-import-position, wrong-import-order, ungrouped-imports
# if you are running it from the command line you will need the following
# lines to set the project root to the working directory

# import sys
# sys.path.append({project_root})

from collections import Counter
import time
import numpy as np
from PIL import Image

from keras.losses import categorical_crossentropy
from keras.optimizers import Adam
from keras.models import Model

from qilin.config import tessdata_dir
from qilin.splitter.cam import get_model
from qilin.splitter.splitter import split_image
from qilin.characteranalysis.tesseract import analyse

filename = "NZCGMJv004i001d19520801f0018p001.tif"

# Headline types (144, 144)
# Subheading types (108, 108)
# Subheading types (72, 108)
# Body types (72, 72)
size_names = ["Noise", "Body", "Subheading", "Headline"]

img_arr = np.asarray(Image.open(filename).convert('L'), dtype='int32')

def model_getter(size):
    model = get_model(size)
    model.load_weights("checkpoints/conv_cam_incept_aux_net.h5")
    model = Model(inputs=model.inputs, outputs=model.outputs[-1])
    model.compile(optimizer=Adam(lr=0.01), loss=categorical_crossentropy)
    return model

start = time.time()
sections_by_size = split_image(
    img_arr,
    model_getter,
    size_names=size_names,
    num_splits=4,
    debugging=True
)
end = time.time()
print('time:', end - start)

for name in size_names:
    if name not in sections_by_size:
        continue
    for idx, split_img in enumerate(sections_by_size[name]):
        Image.fromarray((split_img).astype('uint8'), 'L').save("out_%s_%d.png" % (name, idx))

tessdata_dir_config = '--tessdata-dir \"%s\"' % tessdata_dir

frequency_by_size = {
    "Body": Counter({}),
    "Subheading": Counter({}),
    "Headline": Counter({})
}
for size_name, sections in sections_by_size.items():
    frequency_by_size[size_name] += analyse(tessdata_dir_config, sections)

print(frequency_by_size)
