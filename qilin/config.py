fonts_locations = 'fonts/'

# When rnning in parallel this variable should stay as "./"
# If running linearly this variable would need to change - navigate to the
# directory which contains 'tessdata' and run the command 'pwd' to find
# the value you'd need to set this to
tessdata_dir = "./"
