#!/usr/bin/env python3

from PIL import Image # pip install Pillow
import requests

def figshareGET(token, endpoint):
    url = "https://api.figshare.com/v2/" + endpoint
    headers = {'authorization': 'token {}'.format(token)}
    response = requests.get(url, headers=headers)
    response.raise_for_status()
    return response

def figshareImageDownload(token, f):
    url = "https://ndownloader.figshare.com/files/{}".format(f)
    headers = {'authorization': 'token {}'.format(token)}
    response = requests.get(url, headers=headers, stream=True)
    response.raise_for_status()
    response.raw.decode_content = True
    return Image.open(response.raw)
