"""Cam Trainer
This script will train a convolution network to classify the size of a
given printed chinese character

It requires the four training sets of data one for each size of character
and the fourth for non characters.
"""

# pylint: disable=wrong-import-position, wrong-import-order, ungrouped-imports
# if you are running it from the command line you will need the following
# lines to set the project root to the working directory

# import sys
# sys.path.append({project_root})

import os
import random
import numpy as np
from PIL import Image
from scipy import ndimage

from keras.utils import to_categorical

from qilin.splitter.cam import get_model

def generate_examples(length, size, base_images, allow_full_white=False):
    data_list = []
    random_generator = random.Random()

    areas = [base_image.shape[0] * base_image.shape[1] for base_image in base_images]
    images = random.choices(base_images, weights=areas, k=length)

    for base_image in images:
        if size[0] > base_image.shape[0] or size[1] > base_image.shape[1]:
            continue
        while True:
            x = random_generator.randrange(0, base_image.shape[0] - size[0])
            y = random_generator.randrange(0, base_image.shape[1] - size[1])

            img = base_image[x:x + size[0], y:y + size[1]]
            if allow_full_white or np.mean(img) <= 0.99:
                break

        sx = ndimage.sobel(img, axis=0, mode='constant')
        sy = ndimage.sobel(img, axis=1, mode='constant')
        sob = np.hypot(sx, sy)

        data_list.append(sob)
        sys.stdout.write("\r%d / %d" % (len(data_list), length))
        sys.stdout.flush()
    print("")
    return np.array(data_list)

def _load_files(data_root):
    files = os.listdir(data_root)
    imgs = []
    for file in files:
        img = np.array(Image.open(data_root + '/' + file).convert('L')) / 255
        img[img < 0.5] = 0
        img[img >= 0.5] = 1
        imgs.append(img)
    return imgs

def _load_images():
    return (
        _load_files('resources/splitter_ads'),
        _load_files('resources/splitter_72_72'),
        _load_files('resources/splitter_72_108'),
        _load_files('resources/splitter_144_144')
    )

if __name__ == "__main__":
    imgs_ads, imgs_72_72, imgs_72_108, imgs_144_144 = _load_images()

    size_sample = (72, 72, 1)
    # size = (72, 108, 1)
    # size = (108, 108, 1)
    # size = (144, 144, 1)

    print("Non Chars: ")
    ex_ads = generate_examples(32000, size_sample[:2], imgs_ads, allow_full_white=True)
    ex_ads = np.expand_dims(ex_ads, 3)
    class_ads = np.full((ex_ads.shape[0], 1), 0)

    print("72 72: ")
    ex_72_72 = generate_examples(32000, size_sample[:2], imgs_72_72)
    ex_72_72 = np.expand_dims(ex_72_72, 3)
    class_72_72 = np.full((ex_72_72.shape[0], 1), 1)

    print("72 108: ")
    ex_72_108 = generate_examples(32000, size_sample[:2], imgs_72_108)
    ex_72_108 = np.expand_dims(ex_72_108, 3)
    class_72_108 = np.full((ex_72_108.shape[0], 1), 2)

    print("144 144: ")
    ex_144_144 = generate_examples(32000, size_sample[:2], imgs_144_144)
    ex_144_144 = np.expand_dims(ex_144_144, 3)
    class_144_144 = np.full((ex_144_144.shape[0], 1), 3)

    print("Generated:")
    print("Ads:", class_ads.shape[0])
    print("72_72:", class_72_72.shape[0])
    print("72_108:", class_72_108.shape[0])
    print("144_144:", class_144_144.shape[0])

    X = np.concatenate((ex_ads, ex_72_72, ex_72_108, ex_144_144), 0)
    Y = np.concatenate((class_ads, class_72_72, class_72_108, class_144_144), 0)
    Y = to_categorical(Y)

    model = get_model(size_sample)

    model.summary()

    model.fit(X, [Y, Y, Y, Y], batch_size=256, epochs=200, validation_split=0.2)
    model.save_weights("checkpoints/conv_cam_incept_aux_net.h5")
