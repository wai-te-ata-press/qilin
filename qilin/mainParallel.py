"""The Pipeline
This file pulls together several modules into one Pipeline

This is achieved in parallel via the "runPipeline" function.

The results of that function are collected and processed
in order to output three .html files containing frequencies of the
detected characters. One file for each character size.

These files will be named
- frequency_table_Body.html
- frequency_table_Headline.html
- frequency_table_Subheading.html
"""


#pylint: disable=no-name-in-module
import os
from collections import Counter
from functools import reduce

from datetime import datetime
from keras import Model
from keras.losses import categorical_crossentropy
from keras.optimizers import Adam
from pyspark import SparkContext, SparkConf
from config import tessdata_dir
from characteranalysis.tesseract import writeHTML
from characteranalysis.join import joinMultiple
from figshare.figsharing import figshareGET

# Setting environment variables to make sure they use Python 3
os.environ["PYSPARK_PYTHON"] = "/usr/bin/python3"
os.environ["PYSPARK_DRIVER_PYTHON"] = "/usr/bin/python3"

number_of_workers = 4

def runPipeline(issue_number):
    """
    This is the function that is run in parallel on each worker - it is called seperately
    for each newspaper issue.
    This function is responsible for downloading and splitting the pages of the issue, and
    identifying characters and their frequencies.
    :param issue_number: the figshare identifier for the issue being processed
    """

    # The following imports need to be placed inside this method so that the required packages
    # are imported on the worker nodes.

    #pylint: disable=redefined-outer-name,reimported,no-name-in-module,redundant-keyword-arg

    import numpy as np

    from splitter import split_image
    from tesseract import analyse
    from figsharing import figshareGET
    from figsharing import figshareImageDownload
    from cam import get_model
    from PIL import Image

    size_names = ["Noise", "Body", "Subheading", "Headline"]
    # --psm 7    or 10
    tessdata_dir_config = '--tessdata-dir \"%s\"' % tessdata_dir

    # Get the token that should have been exported as an environment variable
    token = os.environ['figsharePersonalToken']

    # Saves a list of references to each page in this issue
    files = figshareGET(token, 'account/articles/{}/files'.format(issue_number)).json()

    frequency_by_size = {
        "Body": Counter({}),
        "Subheading": Counter({}),
        "Headline": Counter({})
    }

    count = 0

    for page in files:
        # Remove this check to run all pages -
        # it currently only processes 1 page of the given issue
        if (count >= 1):
            break
        count += 1

        # Each page object contains a variety of information for that page. We convert
        # it into a dictionary to easily extract the id
        page = dict(page)

        # Use the page id and the api token to download the file for that page
        im = figshareImageDownload(token, page['id'])

        # Convert the image into an array to prepare it for splitting
        img_arr = np.asarray(im.convert('L'), dtype='int32')

        def model_getter(size):
            model = get_model(size)
            model.load_weights("conv_cam_incept_aux_net.h5")
            model = Model(inputs=model.inputs, outputs=model.outputs[-1])
            model.compile(optimizer=Adam(lr=0.01), loss=categorical_crossentropy)
            return model


        sections_by_size = split_image(img_arr, model_getter, size_names, 4, debugging=True)
        for size_name, sections in sections_by_size.items():
            frequency_by_size[size_name] += analyse(tessdata_dir_config, sections)

    return frequency_by_size

def findFolders(token):
    """
    Given the figshare api token, this function returns a list of the issue ids
    """
    folders = []
    issues = figshareGET(token, '/account/collections/{}/articles'.format(4235018)).json()
    for issue in issues:
        issue = dict(issue)
        folders.append("{}\n".format(issue['id']))
    return folders

print("====================")
print("Pipeline started at:")
print(datetime.now().time())
print("====================")

# Configure spark context, including adding the environment variables to the context
# That is necessary so that the worker nodes have access to that variable
conf = SparkConf().setAppName("Chinese_Analytics").setMaster("yarn"+
    "").setExecutorEnv('figsharePersonalToken', os.environ['figsharePersonalToken'])
sc = SparkContext(conf=conf)

# Add modules and files to the spark context so that worker nodes can access them
sc.addPyFile("qilin/characteranalysis/tesseract.py")
sc.addPyFile("qilin/splitter/splitter.py")
sc.addPyFile("qilin/figshare/figsharing.py")
sc.addPyFile("qilin/train_cam.py")
sc.addPyFile("qilin/splitter/cam.py")
sc.addFile("checkpoints/conv_cam.h5")
sc.addFile("checkpoints/conv_cam_incept_aux_net.h5")
sc.addFile("qilin/characteranalysis/tessdata/chi_tra.traineddata")

ls = findFolders(os.environ['figsharePersonalToken'])
lis = []
lis.append(ls[0])
#lis.append(ls[1])
#lis.append(ls[2])
#lis = ["1", "2"]
a = sc.parallelize(lis, number_of_workers)
op = a.map(runPipeline).collect()
#print(op)
#print(op[0].len)

frequency_by_size = reduce(joinMultiple, op)

print(frequency_by_size)

for size_name in frequency_by_size:
    writeHTML(frequency_by_size[size_name], "frequency_table_%s.html" % size_name)
