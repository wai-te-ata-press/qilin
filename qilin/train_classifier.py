# pylint: disable=wrong-import-position, wrong-import-order, ungrouped-imports
# if you are running it from the command line you will need the following
# lines to set the project root to the working directory

# import sys
# sys.path.append({project_root})

import numpy as np

from qilin.splitter.generate_unicode import get_training_set, ch_common

from keras.models import Model
from keras.layers import Input, Conv2D, Flatten, Dense, Dropout, AveragePooling2D, add, Activation
from keras.activations import softmax, relu
from keras.losses import categorical_crossentropy
from keras.optimizers import SGD
from keras.utils import to_categorical
from keras.metrics import categorical_accuracy

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder

def get_model(shape, classes) -> (Model, Model, Model):
    input_tensor = Input(shape=shape)

    x = Conv2D(32, kernel_size=3, strides=1, padding="same")(input_tensor)
    x = Conv2D(32, kernel_size=3, strides=1, padding="same")(x)
    x = AveragePooling2D()(x)
    x = Dropout(0.15)(x)

    out1 = Flatten()(x)
    out1 = Dense(classes, activation=softmax)(out1)

    x = Conv2D(32, kernel_size=3, strides=1, padding="same")(x)
    x = Conv2D(32, kernel_size=3, strides=1, padding="same")(x)
    x = Dropout(0.15)(x)

    out2 = Flatten()(x)
    out2 = Dense(classes, activation=softmax)(out2)

    x = Conv2D(32, kernel_size=3, strides=1, padding="same")(x)
    x = Conv2D(32, kernel_size=3, strides=1, padding="same")(x)
    x = AveragePooling2D()(x)
    x = Dropout(0.15)(x)

    out3 = Flatten()(x)
    out3 = Dense(classes, activation=softmax)(out3)

    x = Conv2D(32, kernel_size=3, strides=1, padding="same")(x)
    x = Conv2D(32, kernel_size=3, strides=1, padding="same")(x)
    x = Dropout(0.15)(x)

    out4 = Flatten()(x)
    out4 = Dense(classes, activation=softmax)(out4)

    x = Conv2D(32, kernel_size=3, strides=1, padding="same")(x)
    x = Conv2D(32, kernel_size=3, strides=1, padding="same")(x)
    x = AveragePooling2D()(x)
    x = Dropout(0.15)(x)

    out5 = Flatten()(x)
    out5 = Dense(classes, activation=softmax)(out5)

    aux_model = Model(inputs=input_tensor, outputs=[out1, out2, out3, out4, out5])

    aux_model.compile(
        loss=categorical_crossentropy,
        optimizer=SGD(lr=1e-3, momentum=0.5),
        metrics=[categorical_accuracy]
    )

    return aux_model


if __name__ == "__main__":
    SIZE = (32, 32)

    data_1, labels_1 = get_training_set(SIZE, ch_common, font_to_use='wt001')
    data_2, labels_2 = get_training_set(SIZE, ch_common, font_to_use='wt002')
    data_3, labels_3 = get_training_set(SIZE, ch_common, font_to_use='wt006')
    data_4, labels_4 = get_training_set(SIZE, ch_common, font_to_use='wt011')
    data_5, labels_5 = get_training_set(SIZE, ch_common, font_to_use='wt034')
    data_6, labels_6 = get_training_set(SIZE, ch_common, font_to_use='wt064')
    data_7, labels_7 = get_training_set(SIZE, ch_common, font_to_use='wts11')
    data_8, labels_8 = get_training_set(SIZE, ch_common, font_to_use='wts55')

    data = np.concatenate([data_1, data_2, data_3, data_4, data_5, data_6, data_7, data_8], axis=0)
    data = np.expand_dims(data, axis=3)

    labels = np.concatenate(
        [labels_1, labels_2, labels_3, labels_4, labels_5, labels_6, labels_7, labels_8],
        axis=0
    )
    label_encoder = LabelEncoder()
    labels = label_encoder.fit_transform(labels)
    data_y = to_categorical(labels, num_classes=np.unique(labels).shape[0])

    model = get_model(data.shape[1:], data_y.shape[1])
    model.summary()

    train_x, test_x, train_y, test_y = train_test_split(data, data_y, test_size=0.2)
    train_y = [train_y, train_y, train_y, train_y, train_y]
    test_y = [test_y, test_y, test_y, test_y, test_y]

    model.fit(train_x, train_y, batch_size=24, epochs=30, validation_data=(test_x, test_y))

    model.save('models/googleNet_ocr.h5')
