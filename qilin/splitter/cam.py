import numpy as np

from keras.models import Model
from keras.activations import relu, softmax
from keras.losses import categorical_crossentropy
from keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dense, MaxPooling2D, Input, add
from keras.optimizers import SGD
from keras.metrics import categorical_accuracy

from keras import backend as K

def get_model(shape):

    input_layer = Input(shape=shape)

    x = Conv2D(16, kernel_size=3, padding="same", activation=relu)(input_layer)
    c1 = Conv2D(16, kernel_size=1, padding="same", activation=relu)(x)
    c3 = Conv2D(16, kernel_size=3, padding="same", activation=relu)(x)
    c5 = Conv2D(16, kernel_size=5, padding="same", activation=relu)(x)
    m = MaxPooling2D(pool_size=(3, 3), strides=(1, 1), padding='same')(x)
    x = add([c1, c3, c5, m])

    x = MaxPooling2D()(x)
    x = Dropout(0.5)(x)

    out1 = GlobalAveragePooling2D()(x)
    out1 = Dense(4, activation=softmax)(out1)

    x = Conv2D(32, kernel_size=3, padding="same", activation=relu)(x)
    c1 = Conv2D(32, kernel_size=1, padding="same", activation=relu)(x)
    c3 = Conv2D(32, kernel_size=3, padding="same", activation=relu)(x)
    c5 = Conv2D(32, kernel_size=5, padding="same", activation=relu)(x)
    m = MaxPooling2D(pool_size=(3, 3), strides=(1, 1), padding='same')(x)
    x = add([c1, c3, c5, m])

    x = MaxPooling2D()(x)
    x = Dropout(0.5)(x)

    out2 = GlobalAveragePooling2D()(x)
    out2 = Dense(4, activation=softmax)(out2)

    x = Conv2D(64, kernel_size=3, padding="same", activation=relu)(x)
    c1 = Conv2D(64, kernel_size=1, padding="same", activation=relu)(x)
    c3 = Conv2D(64, kernel_size=3, padding="same", activation=relu)(x)
    c5 = Conv2D(64, kernel_size=5, padding="same", activation=relu)(x)
    m = MaxPooling2D(pool_size=(3, 3), strides=(1, 1), padding='same')(x)
    x = add([c1, c3, c5, m])

    x = MaxPooling2D()(x)
    x = Dropout(0.5)(x)

    out3 = GlobalAveragePooling2D()(x)
    out3 = Dense(4, activation=softmax)(out3)

    x = Conv2D(128, kernel_size=3, padding="same", activation=relu)(x)
    c1 = Conv2D(128, kernel_size=1, padding="same", activation=relu)(x)
    c3 = Conv2D(128, kernel_size=3, padding="same", activation=relu)(x)
    c5 = Conv2D(128, kernel_size=5, padding="same", activation=relu)(x)
    m = MaxPooling2D(pool_size=(3, 3), strides=(1, 1), padding='same')(x)
    x = add([c1, c3, c5, m])

    out4 = GlobalAveragePooling2D()(x)
    out4 = Dense(4, activation=softmax)(out4)

    model = Model(inputs=[input_layer], outputs=[out1, out2, out3, out4])
    model.compile(
        optimizer=SGD(lr=0.01),
        loss=categorical_crossentropy,
        metrics=[categorical_accuracy]
    )
    return model


def get_activations(model: Model, img_arr: np.ndarray) \
        -> (np.ndarray, np.ndarray):
    """
    Generates class activation maps.

    The model requires the final three layers to be a Conv layer,
    a Global pooling layer then a Dense layer

    :param model: the Model to use
    :param img_arr: the image to use assumes sorbel edge detection has already being done
    :return: returns 4 np.ndarrays
    """
    width, height, _ = img_arr.shape

    class_weights = model.layers[-1].get_weights()[0]
    final_conv_layer = model.layers[-3]

    get_output = K.function([model.layers[0].input], [final_conv_layer.output])
    conv_outputs = get_output([np.array([img_arr])])[0]
    conv_outputs = conv_outputs[0, :, :, :]

    # Create the class activation maps for character and not character.
    cam_ad = np.zeros(dtype=np.float32, shape=conv_outputs.shape[0:2])
    for i, w in enumerate(class_weights[:, 0]):
        cam_ad += w * conv_outputs[:, :, i]
    cam_72_72 = np.zeros(dtype=np.float32, shape=conv_outputs.shape[0:2])
    for i, w in enumerate(class_weights[:, 1]):
        cam_72_72 += w * conv_outputs[:, :, i]
    cam_72_108 = np.zeros(dtype=np.float32, shape=conv_outputs.shape[0:2])
    for i, w in enumerate(class_weights[:, 2]):
        cam_72_108 += w * conv_outputs[:, :, i]
    cam_144_144 = np.zeros(dtype=np.float32, shape=conv_outputs.shape[0:2])
    for i, w in enumerate(class_weights[:, 3]):
        cam_144_144 += w * conv_outputs[:, :, i]

    return (cam_ad, cam_72_72, cam_72_108, cam_144_144)
