from typing import Dict, List

import numpy as np
from PIL import Image, ImageDraw
from scipy import ndimage
from skimage.measure import label, regionprops
import cv2


def _caculate_from_splits(num_splits: int, model_getter, img, classes)\
        -> (np.ndarray, np.ndarray, np.ndarray, np.ndarray):
    """
    Splits the image into smaller sections so that the networks
    can fit in the ram of a reasonable computer.
    :param num_splits: The number of times to split the image
    :param model_getter: A function that will build the models of the right size
    :param img: The image to split
    :return: The class activation maps joined back together
    """
    from cam import get_activations
    split_size = int(img.shape[1] / num_splits)
    splits = []
    for split_idx in range(num_splits):
        split_from = split_idx * split_size
        split_to = (split_idx + 1) * split_size

        img_split = img[:, split_from:split_to]
        splits.append(get_activations(model_getter(img_split.shape), img_split))

    cams = [None for _ in range(classes)]

    # Join splits back together
    for split_idx, split in enumerate(splits):
        for idx, cam in enumerate(split):
            if split_idx == 0:
                cams[idx] = cam
                continue
            cams[idx] = np.concatenate([cams[idx], cam], axis=1)

    # resize to match input image
    for idx, cam in enumerate(cams):
        cams[idx] = cv2.resize(cam, (img.shape[1], img.shape[0]))

    return tuple(cams)


def split_image(img_arr: np.ndarray, model_getter, size_names, num_splits, debugging=False)\
        -> Dict[str, List[np.ndarray]]:
    """
    Splits the image into multiple sections with a label of the size of the
    characters in the sections

    :param img_arr: assumes a (N x M) matrix with values from 0 to 255
    :param model_getter: a function to build the models
    :param size_names: the labels for the different sizes
    :param num_splits: number of times to split the image so that the models if in RAM
    :param debugging: Whether to save the debugging output.
    :return: a dictonary for size_name to list of image sections
    """
    img_arr = img_arr.astype('float64') / 255
    img_arr[img_arr < 0.5] = 0
    img_arr[img_arr >= 0.5] = 1

    sx = ndimage.sobel(img_arr, axis=0, mode='constant')
    sy = ndimage.sobel(img_arr, axis=1, mode='constant')
    sob = np.hypot(sx, sy)

    sob = np.expand_dims(sob, 2)

    print("Predicting...")

    cam_ad, cam_72_72, cam_72_108, cam_144_144 = \
        _caculate_from_splits(num_splits, model_getter, sob, 4)

    # Some post processing on the activation maps to get cleaner sections.
    cam_ad = ndimage.gaussian_filter(cam_ad, sigma=30)
    cam_72_72 = ndimage.gaussian_filter(cam_72_72, sigma=25)
    cam_72_108 = ndimage.gaussian_filter(cam_72_108, sigma=15)
    cam_72_108 = ndimage.maximum_filter(cam_72_108, size=25)
    cam_144_144 = ndimage.gaussian_filter(cam_144_144, sigma=15)
    cam_144_144 = ndimage.maximum_filter(cam_144_144, size=20)

    cam = np.array([cam_ad, cam_72_72, cam_72_108, cam_144_144])
    cam = np.argmax(cam, axis=0)

    if debugging:
        heatmap = cv2.applyColorMap(np.uint8((255 / 3) * cam), cv2.COLORMAP_JET)
        cv2.imwrite('cam_sections.png', heatmap)

    cam_labeled = label(cam, connectivity=1)
    regions = regionprops(cam_labeled)

    print('Labeling Regions...')

    overlay_img = None
    overlay_drawable = None
    if debugging:
        overlay_img = Image.new('RGBA', (img_arr.shape[1], img_arr.shape[0]), (0, 0, 0, 0))
        overlay_drawable = ImageDraw.Draw(overlay_img)

    print('Splitting...')
    char_images = {}
    for size in size_names:
        char_images[size] = []

    for reg in regions:
        bbox = reg.bbox
        if reg.filled_area <= 134 * 134:
            continue

        section_activation = cam[bbox[0]:bbox[2], bbox[1]:bbox[3]].copy()
        section_labeled = cam_labeled[bbox[0]:bbox[2], bbox[1]:bbox[3]]

        section = img_arr[bbox[0]:bbox[2], bbox[1]:bbox[3]].copy()
        section = section

        section_activation[section_labeled != reg.label] = -1
        section_activation = section_activation.flatten()
        section_activation = section_activation[section_activation != -1]

        section_type = np.argmax(np.bincount(section_activation))
        section[section_labeled != reg.label] = 1

        if size_names[section_type] == 'Noise':
            continue

        if debugging:
            overlay_drawable.rectangle(
                (bbox[1], bbox[0], bbox[3], bbox[2]),
                fill=(255, 0, 0, 100),
                outline='red'
            )

        char_images[size_names[section_type]].append(section * 255)

    if debugging:
        src_img = Image.fromarray((img_arr * 255).astype('uint8'), 'L').convert('RGBA')
        Image.alpha_composite(src_img, overlay_img)\
            .convert('RGB')\
            .save("predictioned_regions.jpg")

    char_images.pop('Noise')
    return char_images
