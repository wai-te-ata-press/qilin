import os
import sys

import numpy as np
from PIL import Image, ImageDraw, ImageFont
from qilin.config import fonts_locations

# Block                                   Range       Comment
# CJK Unified Ideographs                  4E00-9FFF   Common
# CJK Unified Ideographs Extension A      3400-4DBF   Rare
# CJK Unified Ideographs Extension B      20000-2A6DF Rare, historic
# CJK Unified Ideographs Extension C      2A700-2B73F Rare, historic
# CJK Unified Ideographs Extension D      2B740-2B81F Uncommon, some in current use
# CJK Unified Ideographs Extension E      2B820-2CEAF Rare, historic
# CJK Compatibility Ideographs            F900-FAFF   Duplicates, unifiable
#      variants, corporate characters
# CJK Compatibility Ideographs Supplement 2F800-2FA1F Unifiable variants

ch_common = range(0x4e00, 0x9fff)
ch_extA = range(0x3400, 0x4dbf)
ch_extB = range(0x20000, 0x2a6df)
ch_extC = range(0x2a700, 0x2b73f)
ch_extD = range(0x2b740, 0x2b81f)
ch_extE = range(0x2b820, 0x2ceaf)
eng_basic = range(0x0020, 0x007f)

def __get_fonts(size):
    """
    This assumes the characters are square. This assumption does not hold however
    in this case it is unavoidable so should just use the largest then resize latter.
    :param size: Tuple (x, y) the size of the characters to get from the font
    :return: A map of font names to their objects
    """
    font_names = os.listdir(fonts_locations)
    fonts = {}

    for font_name in font_names:
        fonts[repr(font_name.split('.')[0]).strip('\'')] = ImageFont.truetype(
            fonts_locations + font_name,
            size=size[0]
        )

    return fonts

def get_training_set(size, unicode_range, dataset_size=-1, font_to_use=None):
    """
    Generates a training set of unicode characters from given font files

    :param size: Tuple (x, y) The dimensions of the images
    :param unicode_range: ust the constants in this file
    :param dataset_size: the size of the dataset per font to retrieve.
        Use -1 for max.
    :param font_to_use: the name of the file to use for the font. No extension.
    :return: nd.array() dim=(n, size[0], size[1])
    """
    fonts = __get_fonts(size)
    data_list = []
    label_list = []
    sys.stdout.write("\r0 / %d" % (dataset_size))
    sys.stdout.flush()
    i = 0
    for font_name, font in fonts.items():
        if font_to_use is not None and font_name != font_to_use:
            continue
        for character_code in unicode_range:
            character = chr(character_code)
            image = Image.new('RGB', size)
            draw = ImageDraw.Draw(image)
            draw.rectangle([(0, 0), size], fill=(255, 255, 255))
            draw.text((0, 0), character, font=font, fill=0)
            image_arr = np.array(image.convert('L'))
            if image_arr.mean() == 255:  # Remove completely white images
                continue
            data_list.append(image_arr)
            label_list.append(character_code)
            i += 1
            if dataset_size != -1 and i >= dataset_size:
                break
            sys.stdout.write("\r%d / %d" % (i, dataset_size))
            sys.stdout.flush()
        else:
            continue
        break
    data = np.array(data_list)
    labels = np.array(label_list)
    print("")
    print("Generated %d instances" % data.shape[0])
    return data, label_list

def save_training_set(size, dataset_size=-1):
    """
    Generates a training set from the fonts and saves them to the file system.
    deprecated
    :param size: Tuple (x, y) The dimensions of the images
    :param dataset_size: How many images to save per font. -1 for total.
    :return: None
    """
    fonts = __get_fonts(size)

    if not os.path.exists('unicodetraining/'):
        os.mkdir('unicodetraining/')
    if not os.path.exists('unicodetraining/%d_%d' % size):
        os.mkdir('unicodetraining/%d_%d' % size)

    for font_name, font in fonts.items():
        i = 0
        for character_code in range(0x4e00, 0x9fff):
            character = chr(character_code)

            image = Image.new('RGB', size)
            draw = ImageDraw.Draw(image)
            draw.rectangle([(0, 0), size], fill=(255, 255, 255))
            draw.text((0, 0), character, font=font, fill=0)
            image = image.convert('L')
            image.save(
                ('unicodetraining/%d_%d/' + font_name + '_UTF-' + hex(character_code) + '.png')
                % size
            )
            i += 1
            if dataset_size != -1 and i > dataset_size:
                break

def load_traing_set(size):
    """
    Loads a unicode training set from the sets saved in the unicodetraining dir.
    deprecated
    :param size:
    :return:
    """
    size_name = repr(size[0]) + '_' + repr(size[1])
    images = os.listdir('unicodetraining/' + size_name)
    data = np.empty((len(images), size[0], size[1]), dtype='uint8')
    for index, image_name in enumerate(images):
        image = Image.open('unicodetraining/' + size + '/' + image_name)
        img_arr = np.asarray(image, dtype='uint8')
        img_arr.resize((1, size[0], size[1]))
        data[index] = img_arr
    return data
