import os
import pickle

from sklearn.svm import SVC
from sklearn.cluster import KMeans
from skimage.feature import hog
from PIL import Image
import numpy as np


def train_classifiers(training_set_size: int = 7064,
                      data_root: str = 'resources/catalog_characters/', sub_clusters: int = 4,svcs=1):
    """
    This Trains a KMeans to split the data into {sub_clusters} number of clusters then trains a SVC
    on each cluster to classify the characters.

    :param training_set_size: The number of instances to use in the training set
    :param data_root: Where the file are for the training data
    :param sub_clusters: The number of sub clusters to split the training data into
    :return:
    """
    print('loading')
    print(svcs)
    files = os.listdir(data_root)
    files = files[:training_set_size]
    labels = []
    catalog_data = []
    for file in files:
        base_image = np.array(Image.open(data_root + file)).astype('float32')[0:110, :]
        image = np.ones((110, 131))
        image[:base_image.shape[0], :base_image.shape[1]] = base_image
        catalog_data.append(image)
        labels.append(int(file.split('.')[0]))
    labels = np.array(labels)
    catalog_data = np.array(catalog_data)

    catalog_hog_features = []
    for image in catalog_data:
        flattened, hog_image = hog(image, visualise=True)
        catalog_hog_features.append(flattened)

    catalog_hog_features = np.array(catalog_hog_features)

    print("Done loading")
    print("Training splitter")

    kmean = KMeans(n_clusters=sub_clusters, n_jobs=-2)
    kmean.fit(catalog_hog_features)
    predictions = kmean.predict(catalog_hog_features)

    print("Training classifiers on splits")

    classifiers = []
    for i in range(sub_clusters):
        sub_cluster_mask = predictions == i
        data_x = catalog_hog_features[sub_cluster_mask]
        data_y = labels[sub_cluster_mask]
        ar=[]
        for j in range(svcs):
            start=j*len(data_x)//svcs
            end=(j+1)*len(data_x)//svcs
            s=SVC()
            s.fit(data_x[start:end],data_y[start:end])
            ar.append(s)
        classifiers.append(ar)

    print("Finished")

    return kmean, classifiers


def save(filepath, kmeans, classifiers):
    """
    Saves the kmeans and SVC's to a pickle file
    :param filepath:
    :param kmeans:
    :param classifiers:
    :return:
    """
    with open(filepath, 'wb') as output:
        pickle.dump((kmeans, classifiers), output, pickle.HIGHEST_PROTOCOL)


def load(filepath):
    """
    Loads the kmeans and The SVC's from a pickle file
    :param filepath:
    :return:
    """
    with open(filepath, 'rb') as output:
        kmeans, classifiers = pickle.load(output)
    return kmeans, classifiers


def predict(kmeans: KMeans, classifiers, img: Image, threshold: int = 200) -> tuple:
    """
    Predicts the class of one image.
    The class will match the integer in the filename of the training set rather than
    being a unicode value.

    :param kmeans:
    :param classifiers:
    :param img:
    :param threshold:
    :return:
    """
    img = img.resize((131, 110), Image.ANTIALIAS)

    img_arr = np.array(img)
    img_arr[img_arr < threshold] = 0
    img_arr[img_arr >= threshold] = 1
    image = np.ones((110, 131))
    image[:img_arr.shape[0], :img_arr.shape[1]] = img_arr

    newspaper_hog_features = [hog(image)]

    cluster = kmeans.predict(newspaper_hog_features)[0]
    preds=[]
    for i,c in enumerate(classifiers[0]):
        preds.append(classifiers[cluster][i].predict(newspaper_hog_features)[0])

    return preds
