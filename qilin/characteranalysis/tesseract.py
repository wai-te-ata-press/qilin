from collections import Counter
import pytesseract

# Tesseract and pytesseract need to be installed in order to run this code
# This code is an example of how to run tesseract from within python


def sort_index(ord_list, elem):
    if elem in ord_list:
        return ord_list.index(elem)
    return len(ord_list)


def read_order():
    file = open("character_order.txt", "r", encoding="utf-8-sig")
    ordered_list = []
    for line in file:
        ordered_list.append(line[0])
    return ordered_list


def analyse(tessdata_dir_config, image_list):
    output = ""
    for image in image_list:
        output += pytesseract.image_to_string(image,
           lang='chi_tra', config=tessdata_dir_config)
    return Counter(output)


def writeHTML(freq, filename):
    ord_list = read_order()
    freq = list(freq.items())
    freq.sort(key=lambda x: sort_index(ord_list, x[0]), reverse=False)
    #freq = sorted(freq, key=lambda x: sort_index(ord_list, x), reverse=True)

    h = open(filename, "w", encoding="utf-8-sig")
    h.write("<html>\n")
    h.write("<head>\n")
    h.write("   <meta charset=\"UTF-8\">\n")
    h.write("</head>\n")
    h.write("<table>\n")
    h.write("   <tr>\n")
    h.write("       <th>Unicode</th>\n")
    h.write("       <th>Frequency</th>\n")
    h.write("   </tr>\n")
    for pair in freq:
        h.write("   <tr>\n")
        h.write("       <td style=\"font-size: 50px;\">{}</td>\n".format(pair[0]))
        h.write("   <td style=\"font-size: 50px;\">" + \
            repr(pair[1]) + "</td>\n")
        h.write("   </tr>\n")
    h.write("</table>\n")
    h.write("<style>\n")
    h.write("table, th, td {\n")
    h.write("   border: 1px solid grey;\n")
    h.write("   border-collapse: collapse;\n")
    h.write("   text-align:center;\n")
    h.write("   padding: 5px;\n")
    h.write("}\n")
    h.write("</style>\n")
    h.write("</html>\n")
    print("HTML written to file {}".format(filename))
