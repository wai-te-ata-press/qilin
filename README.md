# Qilin

Frequency Analysis on Scans of Chinese Newspapers.

### Installation

Documentation on installation for the project can be found here:
https://gitlab.com/qilindevs/qilin/wikis/pipeline-installation-instructions

The user manual can be found here: https://gitlab.com/qilindevs/qilin/wikis/user-manual
