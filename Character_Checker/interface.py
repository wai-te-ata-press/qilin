# -*- coding: utf-8 -*-

from tkinter import *
from tkinter import messagebox
import os
# from Tkinter import *
from PIL import Image, ImageTk
import re

files = []
panel = None
current_index = 0
text = None
interface_window = None


def find_checked():
    global files
    f = open("record.csv", "r", encoding="utf-8-sig").readlines()
    for line in f:
        if re.match(r'.*,.*,.*', line):
            filename = line.split(',')[0]

            if filename in files:
                files.remove(filename)


def show_context():
    global current_index, files
    page = 'UTF/' + files[current_index][:5] + ".tiff"
    if not os.path.isfile(page):
        messagebox.showerror("Not Found", "Cannot find page, ensure UTF directory is present")
        return

    context_window = Toplevel()
    image = Image.open(page).resize((877, 620), Image.ANTIALIAS)
    image_container = ImageTk.PhotoImage(image)
    page_panel = Label(context_window, image=image_container)
    page_panel.pack()
    context_window.mainloop()


def next_char():
    global current_index, panel, text, files
    if current_index < len(files):
        image = Image.open('UTF2/' + files[current_index])
        image.resize((115, 115), Image.ANTIALIAS)
        image_container = ImageTk.PhotoImage(image)
        panel.configure(image=image_container)
        panel.image = image_container

        guess = files[current_index][-5]
        text.config(text=guess)
    else:
        find_checked()
        current_index = 0

        print(files)
        if len(files) > 0:
            image = Image.open('UTF2/' + files[current_index])
            image.resize((115, 115), Image.ANTIALIAS)
            image_container = ImageTk.PhotoImage(image)
            panel.configure(image=image_container)
            panel.image = image_container

            guess = files[current_index][-5]
            text.config(text=guess)
        else:
            messagebox.showinfo("Done!", "All images checked!")
            interface_window.destroy()


def set_correct(match):
    global current_index

    h = open("record.csv", "a", encoding="utf-8")
    if match:
        h.write(files[current_index] + "," + files[current_index][-5] + ",1\n")
    else:
        h.write(files[current_index] + "," + files[current_index][-5] + ",0\n")

    h.close()

    current_index += 1
    next_char()


def skip():
    global current_index, panel, text
    current_index += 1
    next_char()


def main():
    global panel, text, files, interface_window

    if not os.path.isdir('UTF2'):
        messagebox.showerror("Directory Missing!",
                             "Please ensure the UTF2 directory is in the same place as this executable"
                             " and is unzipped")
        return

    if not os.path.isfile('record.csv'):
        messagebox.showerror("Record Missing!",
                             "Please ensure the record.csv file is in the same place as this executable")
        return

    files = os.listdir('UTF2')
    files.sort()
    files = [name for name in files if name.endswith(".png")]
    find_checked()

    if current_index >= len(files):
        messagebox.showinfo("Done", "No files remaining to check")
        return

    interface_window = Tk()

    image = Image.open('UTF2/' + files[current_index])
    image = image.resize((115, 115), Image.ANTIALIAS)
    image_container = ImageTk.PhotoImage(image)
    panel = Label(interface_window, image=image_container)
    panel.grid(row=0, column=0)

    text = Label(interface_window, text=files[current_index][-5])
    text.config(font="Arial 70", height=1, width=2)
    text.grid(row=0, column=1)
    text2 = Label(interface_window, text="Do these match?", justify=CENTER)
    text2.grid(row=1, column=0, columnspan=2)
    yes_button = Button(interface_window, text="Yes", command=lambda: set_correct(True))
    no_button = Button(interface_window, text="No", command=lambda: set_correct(False))
    skip_button = Button(interface_window, text="Skip", command=lambda: skip())
    context_button = Button(interface_window, text="Show Page", command=lambda: show_context())

    yes_button.grid(row=2, column=0)
    skip_button.grid(row=3, column=0)
    no_button.grid(row=2, column=1)
    context_button.grid(row=3, column=1)
    text.config(state=DISABLED)
    interface_window.mainloop()


main()
