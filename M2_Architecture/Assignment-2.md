# ENGR 301 Project 7 Architectural Design and Prototype
#### Authors: Inti Resende Albuquerque, Andrew McGhie, Rachel Anderson, Deacon McIntyre, Tomas Brown, Tabitha Byrne

## 1. Introduction

### Client

* Rhys Owen:
    * Office: Room 006, Rankine Brown Building, University Library, Kelburn
    * Email: <rhys.owen@vuw.ac.nz>
* Ya-Wen Ho:
    * Office: Room 006, Rankine Brown Building, University Library, Kelburn
    * Email: <ya-wen.ho@vuw.ac.nz>
* Dr. Sydney Shep:
    * Office: Room 006, Rankine Brown Building, University Library, Kelburn
    * Email: <sydney.shep@vuw.ac.nz>

### 1.1 Purpose

The purpose of the software is to play a small part in a larger project's aim, which is conserving a historical printing method, used by a Chinese agricultural press in New Zealand. The three steps of this software's process include identification of the Chinese characters in the supplied newspapers, frequency analysis on the characters identified in each issue, and HTML output of the maximum number of each character found in a single newspaper issue. This output will help the clients have a better understanding of the number of sorts for each character in their type-set, which is the issue they are currently facing.

### 1.2 Scope

 The software, `Qilin`, will receive a set of Chinese newspapers in TIFF format. It will then categorize each character according to its size and type. Furthermore, it will perform frequency analysis on each value in the found character set, per newspaper issue. Once found, the maximum frequency per issue of each character will be output in HTML format as a frequency table. As a stretch goal, the program may also be able to digitize Chinese texts into a PDF format that can be read by a machine.

Once released as an open source project, the software will be able to be used and modified to digitize other traditional Chinese texts.

### 1.3 Product overview 
#### 1.3.1 Product perspective

The software we are creating will be made up of a few ideas or products already created, but the system as a whole is very similar to OCRopus.

OCRopus is an open source system that analyses and performs optical character recognition on documents. 

As the main features of OCRopus are almost identical to the requirements of the project, the clients initially tried to use this product to analyse their newspapers. 
The main issue they faced was that the OCRopus system was created for Latin-based languages, so it did not perform very well.

Although OCRopus seemed to be the perfect fit on paper, the system lacked these capabilities:

1) Distinguishing and separating characters based on font size.

2) Analysing document from top-to-bottom, instead of reading from left-to-right.

3) Recognising strokes and radicals of the Chinese characters.

Our solution is to create our own version of OCRopus, with a frequency analysis extension that handles the output of the system. 

A similar project was done by students of Stanford University, Their software was made to identify the font of weathered Chinese traditional characters and then reconstruct them. The approach used in their project is fairly similar to ours. A neural network was programmed and trained in order to achieve the results required. 

#### 1.3.2 Product functions

The final product will be able to: (In a parallel environment,)

1) download 60GB of newspaper scans from an external cloud storage server,

2) analyse each page from the newspaper and understand which is body text and which is advertisements/notices,

3) pull the characters that are part of the body from the page,

4) perform character recognition on each character found,

5) perform frequency analysis on the Unicode value found for each character, and

6) output the result as a frequency table in HTML format.

#### 1.3.3 User characteristics   

There are two classes of users for our system. The first is the **client** who is the driving force behind the project's creation. The other user of our system is **the public**, as this is an open source project.

Our client,  the Wai-Te-Ata Press, has gained access to The New Zealand Chinese Growers monthly newspaper. Due to a lack of documentation on the sorts used to print these newspapers, the number of sorts corresponding to each character is unknown. The Wai-Te-Ata Press is running a project to catalog the characters used by the newspaper. Through our software and a number of other projects, the Press hope to restore and categorise the sort to the point they can perform small test runs with the historical equipment.

As this software could potentially be used by other people with similar use cases, it was decided to release this product as an open source product. As our clients have a very specific set of requirements, the finished product will be custom made to their problem. This may cause issues for the open source release as the system will most likely have to be altered to perform with another user's data and output requirements.


#### 1.3.4 Limitations

#### Accuracy Of The Recognition:

The software will not be able to recognize 100% of the characters on the newspapers.
It is possible to achieve a high accuracy, based on other image recognition programs with unclassified data.

#### Quality Scanned Of Data:

Some of the scans of the newspapers are not of high quality, this will affect the performance of our character recognition software.

### Quality Of The Training Data:

Good training data is a crucial factor to have a high accuracy recognition software. For this project, the training data is very specific
and hard to generate. This factor will be a big limitation on the outcome of the software.

## 2. References

[1] ISO/IEC/IEEE 42010:2011, "Systems and software engineering - Architecture description". Iso.org. 2011-11-24. Retrieved from https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=6129467

[2] P. Kruchten, "The 4 plus 1 View Model of architecture," IEEE Software, vol. 12, (6), pp. 42-50, 1995. Available: ProQuest, https://search.proquest.com/docview/215835077/abstract/83E7F267AE294CE3PQ. DOI: http://dx.doi.org/10.1109/52.469759. 

## 3. Architecture

### 3.1 Architectural Viewpoints

The viewpoints identified below broadly define ways in which to look at a system. The architectural views in Section 4 are a result of applying these viewpoints to our software system.[1]

#### Logical Viewpoint

This viewpoint is concerned with the core functionality that the system provides. This includes any services or functional requirements the system fulfils [2].

The logical viewpoint should break the system down into its most important objects or object classes. This can be represented by two kinds of model: a Class Diagram and a State Diagram

#### Process Viewpoint

The process viewpoint takes the main abstractions from the Logical Viewpoint, defines how they fit into the process architecture and how they communicate with each other. This viewpoint is concerned with concurrency, distribution, and the non-functional requirements of the system, for example performance [2].

The models used to describe this viewpoint are the Activity Diagram and the Sequence Diagram.

#### Development Viewpoint
The development viewpoint describes a system in terms of the software modules that can be worked on by developers or small groups of developers. This might involve defining program libraries or packages [2].
 
This viewpoint's concerns relate directly to the development of the software. It can be represented by two kinds of model: the Component Diagram and the Package Diagram.

#### Physical Viewpoint
The physical viewpoint is concerned with how the software maps to hardware, and how the software is deployed. Similar to the process viewpoint, this viewpoint takes into account non-functional requirements like reliability and scalability [2].
It can be represented by a Deployment Diagram.


### 4. Architectural Views

### 4.1 Logical

From the perspective of an end-user, our software should be able to take in a collection
of images, specifically newspapers, written vertically in traditional Chinese. It should then be able to accurately
process these images to correctly identify the individual characters, with the result including
frequency analysis of each character by issue. 

The user will need access to a Microsoft Azure account to run the software, and access to
the documents they wish to run the software on. Processes that run in-between the input of 
the images and the output of the results should not need to be seen or acknowledged by the user.

![](ClassDiagram.jpg)

### 4.2 Development

The software will be excuted on an Apache Spark cluster, using an automated download software, an image segmentation software, 
a character recognition software and a Main file which will handle the interaction between all pieces above.

The Spark master node will perform a division of tasks between the slave nodes using parallel programing.
These will then run the OCR software on a small portion of the given data.

The program on each node will then classify each character and link them to a machine readable version of it.
After the software will then perform a frequency analysis, and return the characters and character count both in a machine
readable format.

The slave nodes will then each return the OCR software output, which will then merge all outcomes from the nodes together.
It will then return all characters and frequencies in a human and machine readable format.


### 4.3 Process

#### System Processes:

Apache spark has a master-slave hierarchy between its different kinds of nodes. A node called the 'head node' is the master, and it takes a task and divides it 
among several slave nodes, called 'workers.' Each worker then performs its portion of the task, and returns its results to the head node.

We use the Pyspark library in Python to start our Spark parallel processes. This means we can simply run a Python file which will
then go and create and use the required Spark processes.

Spark uses an data structure called a Resilient Distributed Dataset (RDD) to make the parallel processing simple. It is a distributed
data structure that allows fast access to the data and provides a single dataset to apply all parallel operations on.

Spark improves the performance of our system by running processes concurrently. It divides the tasks into smaller pieces so that
they can be run at the same time, and also because a smaller task takes less time to process. The node system that Spark uses
provides scalability to our software - it allows the process to be scaled up or down by adding or removing nodes. The amount of
nodes only affects how quickly the system operates, so if a test needs to be run on a small amount of data, the number of nodes
can be reduced to save resources.

Another part of the system is the Hadoop Distributed Filesystem (HDFS). The HDFS will store any files that need to be access by all 
of the worker nodes, such as a file containing the names of all the image files that need to be processed. These image names are 
used by the downloading aspect of the software to download the correct files from the cloud. Then, the workers will simultaneously access 
CloudStore to download the file matching the filename they have been given, and move on to the next step: image segmentation.

Each worker will perform the image segmentation process at the same time as the other workers. This will involve the
workers dividing the image they have been given into groups of characters, to make the character recognition step more accurate.
Each worker will run a custom python script that detects lines in the image, and splits the image along those lines.

Still working concurrently, the workers will pass the results of the splitting step through Google Tesseract.
Google Tesseract will attempt to detect and identify characters in the image segments it has been provided, and return
machine readable text. The returned text is Tesseract's best guess at the characters on the image it is provided.

At this point the head node will collect the results from all of the workers. Each worker returns an object mapping the characters it recognised
to the amount of times that character appeared on the page hat the worker processed. In the head node these maps are all combined into
a single map representing frequencies of characters across all of the processed pages. Finally, this map is made into a frequency
table. This table will serve as a guide to the frequency of the actual characters in the newspaper.

![](ENGR301_A2_ProcessDiagram.png)

### 4.4 Physical

Our system is designed to run on a Spark cluster therefore it will be able to run on a number of different clusters as long as they use Spark. The system will always be working at a layer above the physical layer, however we will be setting up a Spark cluster on the Microsft Azure system to run the OCR on the clients data. Like a normal cluster this will have a manager node and a number of worker nodes. The exact specifications will be modified as required to get the most use out of the system and to keep costs low. We will also be using the hosting service on GitLab.com to deploy the results from our system. The physical architecture of this is hidden as the HTML for the site is from an artifact created when running the continuous deployment system that GitLab automatically looks for.

### 4.5 Scenarios
 
#### A user can run frequency analysis on a set of images using a Spark cluster

The user will upload their images to be analysed to a webdav cloud location. When running, the system parameters, such as the URL, location, password, and username, will be passed to the Spark cluster so it can access the images. The worker nodes used in the Spark cluster will then be able to download and process the images using layout analysis and OCR to turn them into machine-readable data. The head node will then be able to analyse the data and to count the frequencies of the characters to generate the frequency analysis results. 

The continuous deployment service will have to determine which images to do the frequency analysis on so that it can pass the location of each image to the worker nodes in the Spark cluster.

Parts of the system, like the frequency analysis, will be easy to unit test. Components that use machine learning to generate their output, such as layout analysis and OCR, will not be able to have unit tests that make sure they are producing the correct results. Instead, we will have to rely on using validation sets when training so that we can be confident in the accuracy of the machine learning algorithms. However, we can still have acceptance tests to make sure that the outputs of those components are correctly formatted.

#### A user can view the results of the frequency analysis

Since the output is static HTML, there needs to be a component that will be able to 
generate HTML from the results of the frequency analysis. After this has been generated the continuous deployment needs to work with GitLab's hosting service so that it will update the HTML output. Once this has happened the user will be able to view the results of the analysis through a browser on any computer connected to the internet.

![](MainUseCaseDiagram.png)

## 5. Development schedule. 

### 5.1 Schedule

Dates for key project deliverables:

1. Minimum Viable Product (MVP)
	-  15th June 2018 (15/06/2018)
	- We are aiming to release a product to our clients through a secondary GitLab account by the 15th of June that accomplishes the following:
		- Create a Spark cluster through Microsoft that will allow us to pull an image from the CloudStore.
		- This image will then be run through our own code that will split up the image into individual characters.
		- Which will then in turn be run through 'Google Tesseract' an OCR software, this software identify the characters.
		- Finally an output in HTML will show the frequency of every character found.

1. All other deliverables are going to be set up on a weekly basis.
	- Team meets with client weekly and we plan a deliverable for the next week.
	- Deliverable may be a runnable piece of code or a progress report by mouth, etc.

1. Final Delivery
	- 19th October 2018 (19/10/2018)

### 5.2 Budget

*We came to the conclusion that there is no need for any budget as we will not be spending any money on resources or products.*

### 5.3 Procurement


|          Product          |                                                                                                                                                                                                                                                                                               Purpose                                                                                                                                                                                                                                                                                              | 		 Source		  |
|:-------------------------:|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:------------------:|
|            Spark          | The purpose of having Spark as part of our product is to split up the workload for the system. Spark allows us to create multiple nodes and split the workload from an amount of 'head' nodes into many 'slave' nodes that will be able to process the smaller, easy to manage tasks by themselves and report their progress back to the 'head' nodes. |   Apache Services  |
|        CloudStor       | CloudStor allows us to have a secure cloud based environment to store the confidential information supplied by the clients. | AARNet |
|      Google Tessaract     | Google Tesseract is an open source OCR software that will be able to read some of the Chinese characters and return the Unicode equivalents. We will need to train the system to work with our data but it is a good starting point. | Google Open Source |
| Microsoft Azure HDInsight | Azure wasn't part of the initial requirements but was added as we found alternatives to the preliminary requirements. HDInsight allows us to create a VM environment that can produce Spark clusters from any device with an internet connection. This will allow the clients to use the system after our involvement with the system has stopped.  | azure.microsoft.com/Azure‎ |
|           GitLab          |								 As our main GitLab account is private, we have created a secondary GitLab account that our clients have access to. This means we can deliver the product in an open source environment where they can share and download/use the program from anywhere in the world from any device. This isn't a major part of the requirements but the clients would ideally like this to be implemented so they can keep on using the software after the project ends and distribute it to other organisations looking to achieve the same goals as them.								 |       GitLab       |


## 6. Appendices
### 6.1 Assumptions and dependencies 

We will assume that we will have access permissions, as necessary, in order to obtain the scans of the newspapers we will be performing the analysis on. Though the project can be partially completed without access to this we cannot properly test it without access to these scans so we must assume we are able to do so. 

### 6.2 Acronyms and abbreviations

* OCR - Optical Character Recognition, software that identifies text characters from an image
* MVP - Minimum Viable Project
* Spark - A program for managing big data using distributed systems
* Tesseract - A open source OCR program by Google
* HDFS - Hadoop Distributed File System


## 7. Contributions

A one page statement of contributions that lists each member of the
group and what they contributed to this document.

* Inti Resende Albuquerque - Section 1, section 4.2
* Andrew McGhie - Sections 4.4 and 4.5
* Rachel Anderson - Section 3.1
* Deacon McIntyre - Sections 3.2 and 4.3
* Tomas Brown - Section 5
* Tabitha Byrne - Sections 4.1 and 6, proofreading/slight rewording
